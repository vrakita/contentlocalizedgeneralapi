<?php

require __DIR__ . '/vendor/autoload.php';

/**
 * Load order info by id
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $order = $info->loadOrder(2);
    print_r(json_decode($order));
} catch (\Exception $e) {
    echo $e->getMessage();
}

/**
 * Load all language combinations
 */
try {
    $info = new \CLGeneralAPIClient\Translation\Info(__DIR__);
    $combinations = $info->allCombinations();
    print_r(json_decode($combinations));
} catch (\Exception $e) {
    echo $e->getMessage();
}


/**
 * Available options for project delivery
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $delivery = $info->getDelivery();
    print_r(json_decode($delivery));
} catch (\Exception $e) {
    echo $e->getMessage();
}

/**
 * Available PG rating for translated content
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $ratings = $info->getRatings();
    print_r(json_decode($ratings));
} catch (\Exception $e) {
    echo $e->getMessage();
}


/**
 * Check is desired language combination available
 */
try {
    $info = new \CLGeneralAPIClient\Translation\Info(__DIR__);
    $combination = $info->availableTranslation('gb', 'fr');
    print_r(json_decode($combination));
} catch (\Exception $e) {
    echo $e->getMessage();
}


/**
 * Calculate and create project
 */
try {

    $order = new CLGeneralAPIClient\Translation\Order(__DIR__);

    $order->sandbox(true)
        ->log(__DIR__)
        ->setSourceLanguage('gb')
        ->setTranslationLanguage(['de', 'fr'])
        ->setTranslationLanguage('it')
        ->setDelivery(1)
        ->setContent('Hello World')
        ->setProjectName('API Project');

    $calculation = json_decode($order->calculate());

    echo 'Total: ' . $calculation->price;
    echo '<br>';

    $x = $order->create();

    print_r($x);

} catch (\Exception $e) {
    exit($e->getMessage());
}

