<?php namespace CLGeneralAPIClient\Additional;

class Url {

    protected static $test = 'https://dev.contentlocalized.com/api';
    protected static $live = 'https://contentlocalized.com/api';

    public static function getUrl($sandbox = false) {

        return $sandbox ? static::$test : static::$live;

    }

}