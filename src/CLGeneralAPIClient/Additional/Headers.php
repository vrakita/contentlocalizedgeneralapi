<?php namespace CLGeneralAPIClient\Additional;

use CLGeneralAPIClient\Contracts\AuthorizationHeaders;
use Dotenv\Dotenv;

class Headers implements AuthorizationHeaders {

    public function __construct($path) {
        $loader = new Dotenv($path);
        $loader->load();
        $loader->required(['CL_EMAIL', 'CL_PASSWORD', 'CL_SSL']);
    }

    public function makeAuthorizationHeaders() {

        return 'Authorization: Basic '. base64_encode(getenv('CL_EMAIL') . ':' . getenv('CL_PASSWORD'));

    }

    public function getSSL() {

        return getenv('CL_SSL');

    }

}