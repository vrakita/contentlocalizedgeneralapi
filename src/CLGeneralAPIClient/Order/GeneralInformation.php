<?php namespace CLGeneralAPIClient\Order;

use CLGeneralAPIClient\Factory\AuthorizationFactory;
use CLGeneralAPIClient\Factory\DriverFactory;
use CLGeneralAPIClient\Additional\Url;

class GeneralInformation {

    /**
     * Sandbox flag
     *
     * @var bool
     */
    protected $sandBox = false;

    /**
     * Available translation combination URL
     *
     * @var string
     */
    protected $combinationURL = '/combination';

    /**
     * Available ratings URL
     *
     * @var string
     */
    protected $ratingURL = '/ratings';

    /**
     * Available products URL
     *
     * @var string
     */
    protected $productURL = '/products';

    /**
     * Available translation combination URL
     *
     * @var string
     */
    protected $deliverURL = '/delivery';

    /**
     * Load order details
     *
     * @var string
     */
    protected $loadURL = '/order/';

    /**
     * Driver for making request
     *
     * @var
     */
    protected $driver;

    public function __construct($path, $driver = 'curl') {

        $authHeaders = AuthorizationFactory::create($path, 'basic');

        $this->driver = DriverFactory::create($driver);

        $this->driver->setHeaders($authHeaders->makeAuthorizationHeaders())
            ->setHeaders('Content-type: application/json')
            ->post();

        $authHeaders->getSSL() ?: $this->driver->sslOff();

    }

    /**
     * Set sandbox mode
     *
     * @param bool|true $sandbox
     * @return $this
     */
    public function sandbox($sandbox = true) {

        $this->sandBox = $sandbox;

        return $this;

    }

    /**
     * Get delivery options
     *
     * @return mixed
     * @throws \Exception
     */
    public function getDelivery() {

        $this->driver->setUrl(Url::getUrl($this->sandbox()) . $this->deliverURL)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

    /**
     * Get ratings options
     *
     * @return mixed
     * @throws \Exception
     */
    public function getRatings() {

        $this->driver->setUrl(Url::getUrl($this->sandbox()) . $this->ratingURL)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

    /**
     * Get products options
     *
     * @return mixed
     * @throws \Exception
     */
    public function getProducts() {

        $this->driver->setUrl(Url::getUrl($this->sandBox) . $this->productURL)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

    /**
     * Load order by id
     *
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function loadOrder($id = 0) {

        $url = (Url::getUrl($this->sandBox)) . $this->loadURL . $id;

        $this->driver->setUrl($url)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

}