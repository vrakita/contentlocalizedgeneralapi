<?php namespace CLGeneralAPIClient\Files;

use CLGeneralAPIClient\Contracts\FileManager;

class ErrorHandler implements FileManager {

    public function save($path, $content) {

        $fullPath = $this->clearSlash($path) . '/log.txt';

        if( ! is_dir($this->clearSlash($path))) mkdir($this->clearSlash($path), 0777, true);

        $date = date('[Y-m-d H:i:s]');

        @file_put_contents($fullPath, $date . ' ' . $content . "~^~", FILE_APPEND);

    }

    private function clearSlash($path) {

        return rtrim($path, "/");

    }

}