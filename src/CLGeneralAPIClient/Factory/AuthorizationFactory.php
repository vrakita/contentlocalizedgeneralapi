<?php namespace CLGeneralAPIClient\Factory;

use CLGeneralAPIClient\Additional\Headers;

abstract class AuthorizationFactory {

    public static function create($path, $type) {

        switch (strtolower($type)) {
            case 'basic':
                return new Headers($path);
                break;

            default:
                throw new \Exception("Authorization not supported", 1);
                break;
        }

    }

}