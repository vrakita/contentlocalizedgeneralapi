<?php namespace CLGeneralAPIClient\Contracts;

interface Sync {

    /**
     * Set writers email for completed work
     *
     * @param string $email
     * @return $this
     */
    public function setWriter($email);

    /**
     * Set source language of translated string, default value is set to gb
     *
     * @param string $sourceLang
     * @return $this
     */
    public function setSourceLanguage($sourceLang);

    /**
     * Set target language of translated string
     *
     * @param $language
     * @return $this
     */
    public function setTranslationLanguage($language);

    /**
     * Set original string
     *
     * @param string $string
     * @return $this
     */
    public function setString($string);

    /**
     * Set translation for string
     *
     * @param string $string
     * @return $this
     */
    public function setTranslatedString($string);

    /**
     * Make sync call
     *
     * @return mixed
     * @throws \Exception
     */
    public function send();

}