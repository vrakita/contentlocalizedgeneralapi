<?php namespace CLGeneralAPIClient\Contracts;

interface FileManager {

    public function save($path, $content);

}