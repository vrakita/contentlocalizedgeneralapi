<?php namespace CLGeneralAPIClient\Translation;

use CLGeneralAPIClient\Factory\AuthorizationFactory;
use CLGeneralAPIClient\Factory\DriverFactory;
use CLGeneralAPIClient\Additional\Url;

class Info {

    /**
     * Driver for making request
     *
     * @var
     */
    protected $driver;

    /**
     * Sandbox flag
     *
     * @var bool
     */
    protected $sandBox = false;

    /**
     * Available translation combination URL
     *
     * @var string
     */
    protected $availableTranslationURL = '/translation/combination';

    /**
     * All language combinations
     *
     * @var string
     */
    protected $allCombinationsURL = '/language';

    public function __construct($path, $driver = 'curl') {

        $authHeaders = AuthorizationFactory::create($path, 'basic');

        $this->driver = DriverFactory::create($driver);

        $this->driver->setHeaders($authHeaders->makeAuthorizationHeaders())
            ->setHeaders('Content-type: application/json')
            ->post();

        $authHeaders->getSSL() ?: $this->driver->sslOff();

    }

    /**
     * Set sandbox mode
     *
     * @param bool|true $sandbox
     * @return $this
     */
    public function sandbox($sandbox = true) {

        $this->sandBox = $sandbox;

        return $this;

    }

    /**
     * Check is translation combination available
     *
     * @param $fromLanguage
     * @param $toLanguage
     * @return mixed
     * @throws \Exception
     */
    public function availableTranslation($fromLanguage, $toLanguage) {

        $data['from-language']  = $fromLanguage;
        $data['to-language']    = $toLanguage;

        if( ! isset($fromLanguage) || ! isset($toLanguage)) throw new \Exception('Parameter is missing. Request not sent');

        $this->driver->setUrl(Url::getUrl($this->sandBox) . $this->availableTranslationURL)
            ->setData($data)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

    public function allCombinations() {

        $this->driver->setUrl(Url::getUrl($this->sandBox) . $this->allCombinationsURL)
            ->get();

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode() . ': ' . $response);

        return $response;

    }

}