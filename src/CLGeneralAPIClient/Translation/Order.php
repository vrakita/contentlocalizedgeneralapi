<?php namespace CLGeneralAPIClient\Translation;

use CLGeneralAPIClient\Factory\AuthorizationFactory;
use CLGeneralAPIClient\Translation\Validation\Validator;
use CLGeneralAPIClient\Factory\DriverFactory;
use CLGeneralAPIClient\Files\ErrorHandler;
use CLGeneralAPIClient\Additional\Url;


class Order {

    /**
     * Path to directory for logging errors
     *
     * @var bool
     */
    protected $logPath = false;

    /**
     * API endpoint which is going to be added on http://domain.com/api
     *
     * @var string
     */
    protected $url = '/order';

    /**
     * @var string
     */
    protected $calculateURL = '/order/calculate';

    /**
     * Sandbox flag
     *
     * @var bool
     */
    protected $sandBox = false;

    /**
     * Driver for making request
     *
     * @var
     */
    protected $driver;

    /**
     * Project name
     *
     * @var string
     */
    protected $projectName;

    /**
     * Project instructions
     *
     * @var string
     */
    protected $projectInstructions;

    /**
     * Delivery id
     *
     * @var int
     */
    protected $delivery;

    /**
     * Rating id
     *
     * @var int
     */
    protected $rating;

    /**
     * Data array which will be sent with request
     *
     * @var array
     */
    protected $data;

    /**
     * Source language for translating text
     *
     * @var string
     */
    protected $sourceLanguage = 'gb';

    /**
     * Target languages for translating text
     *
     * @var array
     */
    protected $targetLanguages = [];

    /**
     * Text for translating
     *
     * @var string
     */
    protected $content;

    /**
     * Response after request is completed
     *
     * @var
     */
    protected $response;


    public function __construct($path, $driver = 'curl') {

        $authHeaders = AuthorizationFactory::create($path, 'basic');

        $this->driver = DriverFactory::create($driver);

        $this->driver->setHeaders($authHeaders->makeAuthorizationHeaders())
            ->setHeaders('Content-type: application/json')
            ->post();

        $authHeaders->getSSL() ?: $this->driver->sslOff();

    }

    /**
     * Set project name
     *
     * @param string $name
     * @return $this
     */
    public function setProjectName($name) {

        $this->projectName = $name;

        return $this;

    }

    /**
     * Set project name
     *
     * @param string $instructions
     * @return $this
     */
    public function setProjectInstructions($instructions) {

        $this->projectInstructions = $instructions;

        return $this;

    }

    /**
     * Set project delivery option
     *
     * @param int $delivery
     * @return $this
     */
    public function setDelivery($delivery) {

        $this->delivery = $delivery;

        return $this;

    }

    /**
     * Set project rating
     *
     * @param int $rating
     * @return $this
     */
    public function setRating($rating) {

        $this->rating = $rating;

        return $this;

    }

    /**
     * Set source language for translating text
     *
     * @param $sourceLanguage
     * @return $this
     */
    public function setSourceLanguage($sourceLanguage) {

        $this->sourceLanguage = $sourceLanguage;

        return $this;

    }

    /**
     * Set target language of translated string
     *
     * @param $language
     * @return $this
     */
    public function setTranslationLanguage($language) {

        if(is_array($language)) {
            foreach($language as $lang) {
                $this->setTranslationLanguage($lang);
            }
        }
        else $this->targetLanguages[] = $language;

        return $this;

    }

    /**
     * Set translating text
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Set sandbox mode
     *
     * @param bool|true $sandbox
     * @return $this
     */
    public function sandbox($sandbox = true) {

        $this->sandBox = $sandbox;

        return $this;

    }

    protected function setData() {

        $this->data['project-name'] = $this->projectName;
        $this->data['from_id']      = $this->sourceLanguage;
        $this->data['to_id']        = array_unique($this->targetLanguages);
        $this->data['content']      = $this->content;
        $this->data['product']      = 'translation';

        if($this->projectInstructions !== null) $this->data['instructions'] = $this->projectInstructions;
        if($this->delivery !== null)            $this->data['delivery'] = $this->delivery;
        if($this->rating !== null)              $this->data['rating'] = $this->rating;

        Validator::validate($this->data);

        $this->driver->setData($this->data);

    }

    /**
     * Enable error logging
     *
     * @param bool|false $path
     * @return $this
     */
    public function log($path = false) {

        $this->logPath = $path;

        return $this;

    }

    public function calculate() {

        $this->setData();

        $this->driver->setUrl(Url::getUrl($this->sandBox) . $this->calculateURL);

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) {

            if($this->logPath) (new ErrorHandler())->save($this->logPath, $this->driver->getResponseCode() . ': ' . $response . ' * ' . serialize($this->data));

            throw new \Exception('Server error (' . $this->driver->getResponseCode() . '): ' . $response);

        }

        return $response;

    }

    /**
     * Make sync call
     *
     * @return mixed
     * @throws \Exception
     */
    public function create() {

        $this->setData();

        $this->driver->setUrl(Url::getUrl($this->sandBox) . $this->url);

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) {

            if($this->logPath) (new ErrorHandler())->save($this->logPath, $this->driver->getResponseCode() . ': ' . $response . ' * ' . serialize($this->data));

            throw new \Exception('Server error (' . $this->driver->getResponseCode() . '): ' . $response);

        }

        return $response;

    }

}