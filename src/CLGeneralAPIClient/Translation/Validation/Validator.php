<?php namespace CLGeneralAPIClient\Translation\Validation;

class Validator {


    public static function validate($data) {

        if( ! isset($data['from_id']) || $data['from_id'] == '') throw new \Exception('Source language is missing. Request not sent');
        if( ! isset($data['to_id']) || ! is_array($data['to_id'])) throw new \Exception('Target Language is missing. Request not sent');
        if( ! isset($data['project-name']) || $data['project-name'] == '') throw new \Exception('Project name is missing. Request not sent');
        if( ! isset($data['content']) || $data['content'] == '') throw new \Exception('Translation content is missing. Request not sent');

    }

}