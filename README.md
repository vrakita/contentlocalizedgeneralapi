# Content Localized General API #

Content Localized General API is used for calculating order price and ordering new projects. If you do not want to create orders from ContentLocalized website, you can now use API and integrate it in your own system and get instant notification on order completion.

### Installation: ###

Add this to your composer.json

```json
{
    "require": {
        "vrakita/contentlocalizedgeneralapi": "1.*"
    },
    "config": {
        "preferred-install": "dist"
    }
}
```

Update your dependencies
```sh
composer update
```

Create .env file with your ContentLocalized credentials
```sh
CL_EMAIL=mymail@firstbeatmedia.com
CL_PASSWORD=secretpassword
CL_SSL=1
```

### Example of creating new order: ###


```php
<?php

require 'vendor/autoload.php';

try {
    // In Order constructor you have to pass path to directory where .env file with your credentials is placed
    $order = new \CLGeneralAPIClient\Translation\Order(__DIR__);

    $order
        // Your project name
        ->setProjectName('API Project')
        // Content for translation
        ->setContent('Hello World')
        // Set language of your content
        ->setSourceLanguage('gb')
        // Set desired language(s) for your translation
        ->setTranslationLanguage(['de', 'fr'])
        // Option for delivery
        ->setDelivery(1)
        // Use sandbox mode
        ->sandbox(true)
        // Log errors to custom directory
        ->log(__DIR__)
        // Create order
        ->create();

    print_r(json_decode($order));

} catch (\Exception $e) {
    exit($e->getMessage());
}

```

### Example of calculating price and creating creating new order: ###


```php
<?php

require 'vendor/autoload.php';

try {
    // In Order constructor you have to pass path to directory where .env file with your credentials is placed
    $order = new \CLGeneralAPIClient\Translation\Order(__DIR__);

    $calculation = $order
        // Your project name
        ->setProjectName('API Project')
        // Content for translation
        ->setContent('Hello World')
        // Set language of your content
        ->setSourceLanguage('gb')
        // Set desired language(s) for your translation
        ->setTranslationLanguage(['de', 'fr'])
        // Option for delivery
        ->setDelivery(1)
        // Use sandbox mode
        ->sandbox(true)
        // Log errors to custom directory
        ->log(__DIR__);

    // We now have price of our order and if we are good with it we can confirm that order
    $calculation = json_decode($order->calculate());

    // We are OK with the price and we can now create order based on calculate order parameters
    if($calculation->price < 1000) {

        $response = $order->create();

        print_r(json_decode($response));

    }
    

} catch (\Exception $e) {
    exit($e->getMessage());
}

```
### Additional methods ###

```php
<?php

require 'vendor/autoload.php';

/**
 * Load order info by id
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $order = $info->loadOrder(2);
    print_r(json_decode($order));
} catch (\Exception $e) {
    echo $e->getMessage();
}

/**
 * Check is desired language combination available
 */
try {
    $info = new \CLGeneralAPIClient\Translation\Info(__DIR__);
    $combination = $info->availableTranslation('gb', 'fr');
    print_r(json_decode($combination));
} catch (\Exception $e) {
    echo $e->getMessage();
}


/**
 * Load all language combinations
 */
try {
    $info = new \CLGeneralAPIClient\Translation\Info(__DIR__);
    $combinations = $info->allCombinations();
    print_r(json_decode($combinations));
} catch (\Exception $e) {
    echo $e->getMessage();
}


/**
 * Available options for project delivery
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $delivery = $info->getDelivery();
    print_r(json_decode($delivery));
} catch (\Exception $e) {
    echo $e->getMessage();
}

/**
 * Available PG ratings
 */
try {
    $info = new \CLGeneralAPIClient\Order\GeneralInformation(__DIR__);
    $ratings = $info->getRatings();
    print_r(json_decode($ratings));
} catch (\Exception $e) {
    echo $e->getMessage();
}

```